﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Создание списка команд
	/// </summary>
	public class CommandBuilder
	{
		/// <summary>
		/// Временное хранение команд
		/// </summary>
		private readonly List<Command> _commands = new List<Command>();
		
		/// <summary>
		/// Добавить команду
		/// </summary>
		/// <param name="title">Текст</param>
		/// <param name="callback">Callback</param>
		/// <returns>This builder</returns>
		public CommandBuilder Add(string title, Action callback)
		{
			_commands.Add(new Command(title, callback));
			return this;
		}

		/// <summary>
		/// Добавить async команду
		/// </summary>
		/// <param name="title">Текст</param>
		/// <param name="callback">Callback</param>
		/// <returns>This builder</returns>
		public CommandBuilder Add(string title, Func<Task> callback)
		{
			_commands.Add(new Command(title, callback));
			return this;
		}

		/// <summary>
		/// Вернуть список комманд
		/// </summary>
		/// <returns>Список комманд</returns>
		public List<Command> Build()
		{
			return _commands;
		}
	}
}