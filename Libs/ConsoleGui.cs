﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Консольный GUI список
	/// Есть недостаток, количество строк должно быть меньше размера консоли
	/// </summary>
	public static class ConsoleGui
	{
		/// <summary>
		/// Цвета консоли
		/// </summary>
		// Структура, так как предпологается, как временная переменная.
		private readonly struct Colors
		{
			/// <summary>
			/// Background цвет консоли
			/// </summary>
			public readonly ConsoleColor BackgroundColor;
			/// <summary>
			/// Foreground цвет консоли
			/// </summary>
			public readonly ConsoleColor ForegroundColor;

			/// <summary>
			/// Устанавливает цвета
			/// </summary>
			/// <param name="backgroundColor">Background цвет</param>
			/// <param name="foregroundColor">Foreground цвет</param>
			public Colors(ConsoleColor backgroundColor, ConsoleColor foregroundColor)
			{
				BackgroundColor = backgroundColor;
				ForegroundColor = foregroundColor;
			}
		}

		/// <summary>
		/// Позиция в меню
		/// </summary>
		private class Pointer
		{
			/// <summary>
			/// Предел позиции, не включительно
			/// </summary>
			private readonly int _max;
			private int _position;

			/// <summary>
			/// Текущая позиция
			/// </summary>
			public int Position
			{
				get => _position;
				set
				{
					if (value < 0)
					{
						_position = 0;
					}
					else if (value >= _max)
					{
						_position = _max - 1;
					}
					else
					{
						_position = value;
					}
				}
			}

			/// <summary>
			/// Устанавливает максимальную и стартовую позицию
			/// </summary>
			/// <param name="commands">Список команд</param>
			/// <param name="position">Стартовая позиция</param>
			public Pointer(string[] commands, int position = 0)
			{
				_max = commands.Length;
				Position = position;
			}
		}

		/// <summary>
		/// Проверяет достаточно ли места в консоле
		/// иначе выдаёт ArgumentOutOfRangeException
		/// </summary>
		/// <param name="commands">Список команд</param>
		private static void IsEnoughSpace(string[] commands)
		{
			if (commands.Length >= Console.WindowHeight)
			{
				throw new ArgumentOutOfRangeException($"Все команды не помещаются в консоле");
			}
		}

		public static async Task Loop(List<Command> commands)
		{
			await Loop(commands.Select(item => item.Title).ToArray(), async position =>
			{
				var callback = commands[position].Callback;
				if (callback == null)
				{
					Console.WriteLine("Команда не найдена");
					return;
				}
				await callback();
			});
		}

		/// <summary>
		/// Запускает асинхронный цыкл ввода
		/// Если команд больше размера консоли выдаст ArgumentOutOfRangeException
		/// 
		/// Стрелочка вверх, W - Вверх
		/// Стрелочка вниз, S - Вниз
		/// Enter - Выполнить действие
		/// Esc - Выйти из цыкла
		/// </summary>
		/// <param name="commands">Список команд</param>
		/// <param name="pressEnterCallback">Действие при нажатии на Enter</param>
		public static async Task Loop(string[] commands, Func<int, Task> pressEnterCallback)
		{
			IsEnoughSpace(commands);
			Console.Clear();
			Console.CursorVisible = false;
			var pointer = new Pointer(commands);
			PrintMenu(commands, pointer);
			var isBreak = false;
			while (!isBreak)
			{
				var key = Console.ReadKey(true);
				HashSet<int> updatePositions = null;
				switch (key.Key)
				{
					case ConsoleKey.UpArrow:
					case ConsoleKey.W:
						updatePositions = new HashSet<int>();
						updatePositions.Add(pointer.Position);
						updatePositions.Add(--pointer.Position);
						break;
					case ConsoleKey.DownArrow:
					case ConsoleKey.S:
						updatePositions = new HashSet<int>();
						updatePositions.Add(pointer.Position);
						updatePositions.Add(++pointer.Position);
						break;
					case ConsoleKey.Enter:
						Console.Clear();
						PrintMenu(commands, pointer);
						Console.SetCursorPosition(0, commands.Length);
						await pressEnterCallback(pointer.Position);
						break;
					case ConsoleKey.Escape:
						isBreak = true;
						break;
				}
				PrintMenu(commands, pointer, updatePositions);
			}
			Console.Clear();
			Console.CursorVisible = true;
		}

		/// <summary>
		/// Печатает меню в консоль
		/// </summary>
		/// <param name="commands">Список команд</param>
		/// <param name="pointer">Позиция курсора</param>
		/// <param name="updatePositions">Позиции которые нужно обносить или напечатать всё, если null</param>
		private static void PrintMenu(string[] commands, Pointer pointer, HashSet<int> updatePositions = null)
		{
			for (int i = 0; i < commands.Length; i++)
			{
				if (updatePositions == null || updatePositions.Contains(i))
				{
					var command = commands[i];
					WriteLine(command, pointer.Position == i, i);
				}
			}
		}

		/// <summary>
		/// Вывести пункт меню
		/// </summary>
		/// <param name="text">Текст пункта</param>
		/// <param name="inverseColor">Выделять ли пункт</param>
		/// <param name="cursorPosition">Позиция пункта или продолжить на текущей позиции</param>
		private static void WriteLine(string text, bool inverseColor = false, int cursorPosition = -1)
		{
			var defaultColors = new Colors(Console.BackgroundColor, Console.ForegroundColor);

			if (cursorPosition >= 0)
			{
				Console.SetCursorPosition(0, cursorPosition);
			}

			if (inverseColor)
			{
				Console.BackgroundColor = defaultColors.ForegroundColor;
				Console.ForegroundColor = defaultColors.BackgroundColor;
			}

			Console.WriteLine(text);

			if (inverseColor)
			{
				Console.BackgroundColor = defaultColors.BackgroundColor;
				Console.ForegroundColor = defaultColors.ForegroundColor;
			}

			if (cursorPosition >= 0)
			{
				Console.SetCursorPosition(0, cursorPosition);
			}
		}
	}
}