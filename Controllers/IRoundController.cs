﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers
{
	/// <summary>
	/// Контроллер круга
	/// </summary>
	public interface IRoundController
	{
		/// <summary>
		/// Печатать круг
		/// </summary>
		/// <param name="round">Круг</param>
		public virtual void PrintRound(Round round)
		{
			throw new Exception("Метод не реализован");
		}
	}
}