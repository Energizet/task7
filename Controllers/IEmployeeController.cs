﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers
{
	/// <summary>
	/// Контроллер сотрудника
	/// </summary>
	public interface IEmployeeController
	{
		/// <summary>
		/// Печатать сотрудника
		/// </summary>
		/// <param name="employee">Сотрудник</param>
		public virtual void PrintEmployee(Employee employee)
		{
			throw new Exception("Метод не реализован");
		}
	}
}