﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;

namespace Norbit.Crm.Krikunov.Task7.Controllers
{
	/// <summary>
	/// Реализания конторллера круга
	/// </summary>
	public class RoundController : IRoundController
	{
		/// <inheritdoc />
		public void PrintRound(Round round)
		{
			Console.WriteLine($"Радиус = {round.Radius}");
			Console.WriteLine($"Окружность = {round.Circle}");
			Console.WriteLine($"Площадь = {round.S}");
		}

		/// <summary>
		/// Запуск выполнения задачи по кругу
		/// </summary>
		public static async Task Run()
		{
			var roundCase = new RoundUseCase(new RoundController(), new RoundRepository());
			await roundCase.GetAndPrintRound();
		}
	}
}