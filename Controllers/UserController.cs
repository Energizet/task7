﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;

namespace Norbit.Crm.Krikunov.Task7.Controllers
{
	/// <summary>
	/// Реализания конторллера пользователя
	/// </summary>
	public class UserController : IUserController
	{
		/// <inheritdoc />
		public void PrintUser(User user)
		{
			Console.WriteLine(user.ToString());
		}

		/// <summary>
		/// Запуск выполнения задачи по пользователю
		/// </summary>
		public static async Task Run()
		{
			var useCase = new UserUseCase(new UserController(), new UserRepository());
			await useCase.GetAndPrintUser();
		}
	}
}