﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers
{
	/// <summary>
	/// Контроллер пользователя
	/// </summary>
	public interface IUserController
	{
		/// <summary>
		/// Печатать пользователя
		/// </summary>
		/// <param name="user">Пользователь</param>
		public virtual void PrintUser(User user)
		{
			throw new Exception("Метод не реализован");
		}
	}
}