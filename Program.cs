﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Libs;
using Norbit.Crm.Krikunov.Task7.Controllers;

namespace Norbit.Crm.Krikunov.Task7
{
	class Program
	{
		static async Task Main()
		{
			var commands = new CommandBuilder()
				.Add("Напечатать круг", RoundController.Run)
				.Add("Ввести пользователя", UserController.Run)
				.Add("Ввести сотрудника", EmployeeController.Run)
				.Build();

			await ConsoleGui.Loop(commands);
		}
	}
}
