﻿using System;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <summary>
	/// Получение данных пользователя
	/// </summary>
	public interface IUserRepository
	{
		/// <summary>
		/// Получить фамилию
		/// </summary>
		/// <returns>Фамилия</returns>
		public Task<string> GetLastName();
		/// <summary>
		/// Получить имя
		/// </summary>
		/// <returns>Имя</returns>
		public Task<string> GetFirstName();
		/// <summary>
		/// Получить отчество
		/// </summary>
		/// <returns>Отчество</returns>
		public Task<string> GetMiddleName();
		/// <summary>
		/// Получить дату рождения
		/// </summary>
		/// <returns>Дата рождения</returns>
		public Task<DateTime> GetBirthDay();
	}
}