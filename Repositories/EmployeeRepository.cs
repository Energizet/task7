﻿using System;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <inheritdoc cref="IEmployeeRepository"/>
	public class EmployeeRepository : UserRepository, IEmployeeRepository
	{

		/// <inheritdoc />
		public async Task<int> GetExperience()
		{
			return await Task.Run(() =>
			{
				while (true)
				{
					Console.Write("Введите опыт: ");
					var experienceStr = Console.ReadLine();
					if (int.TryParse(experienceStr, out var experience))
					{
						return experience;
					}

					Console.WriteLine("Неверное число");
				}
			});
		}

		/// <inheritdoc />
		public async Task<string> GetPosition()
		{
			return await Task.Run(() =>
			{
				Console.Write("Введите должность: ");
				return Console.ReadLine();
			});
		}
	}
}