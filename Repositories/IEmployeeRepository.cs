﻿using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <summary>
	/// Получение данных сотрудника
	/// </summary>
	public interface IEmployeeRepository : IUserRepository
	{
		/// <summary>
		/// Получить имя
		/// </summary>
		/// <returns>Имя</returns>
		public Task<int> GetExperience();
		/// <summary>
		/// Получить имя
		/// </summary>
		/// <returns>Имя</returns>
		public Task<string> GetPosition();
	}
}