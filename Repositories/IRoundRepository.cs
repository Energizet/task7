﻿using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <summary>
	/// Получение данных круга
	/// </summary>
	public interface IRoundRepository
	{
		/// <summary>
		/// Получить радиус
		/// </summary>
		/// <returns>Радиус</returns>
		public Task<double> GetRadius();
	}
}