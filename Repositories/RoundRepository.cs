﻿using System;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <inheritdoc cref="IRoundRepository"/>/>
	public class RoundRepository : IRoundRepository
	{
		/// <inheritdoc />
		public async Task<double> GetRadius()
		{
			return await Task.Run(() =>
			{
				while (true)
				{
					Console.Write("Введите радиус: ");
					var radiusStr = Console.ReadLine();
					if (int.TryParse(radiusStr, out var radius))
					{
						return radius;
					}

					Console.WriteLine("Неверное число");
				}
			});
		}
	}
}