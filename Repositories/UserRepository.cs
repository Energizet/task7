﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <inheritdoc cref="IUserRepository"/>
	public class UserRepository : IUserRepository
	{
		/// <inheritdoc />
		public async Task<string> GetLastName()
		{
			return await Task.Run(() =>
			{
				Console.Write("Введите фамилию: ");
				return Console.ReadLine();
			});
		}

		/// <inheritdoc />
		public async Task<string> GetFirstName()
		{
			return await Task.Run(() =>
			{
				Console.Write("Введите имя: ");
				return Console.ReadLine();
			});
		}

		/// <inheritdoc />
		public async Task<string> GetMiddleName()
		{
			return await Task.Run(() =>
			{
				Console.Write("Введите отчество: ");
				return Console.ReadLine();
			});
		}

		/// <inheritdoc />
		public async Task<DateTime> GetBirthDay()
		{
			return await Task.Run(() =>
			{
				while (true)
				{
					Console.Write("Введите дату рождения (dd.MM.yyyy): ");
					var dateTimeStr = Console.ReadLine();
					var isDateTime = DateTime.TryParseExact(dateTimeStr, "dd.MM.yyyy", new DateTimeFormatInfo(),
					DateTimeStyles.None, out var dateTime);
					if (isDateTime)
					{
						return dateTime;
					}

					Console.WriteLine("Неверный формат");
				}
			});
		}
	}
}