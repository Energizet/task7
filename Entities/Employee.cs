﻿using System;

namespace Norbit.Crm.Krikunov.Task7.Entities
{
	/// <summary>
	/// Сотрудник
	/// </summary>
	public class Employee : User
	{
		private int _experience;

		/// <summary>
		/// Стаж работы
		/// </summary>
		public int Experience
		{
			get => _experience;
			set
			{
				if (value < 0)
				{
					throw new Exception("Стаж не может быть отрецательным");
				}
				_experience = value;
			}
		}

		/// <summary>
		/// Должность
		/// </summary>
		public string Position { get; set; }

		/// <summary>
		/// Сотрудник по-умолчинию
		/// </summary>
		public Employee()
		{
			Experience = 0;
			Position = "";
		}

		/// <summary>
		/// Сотрудник с указаными полями пользователя
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="birthDay">День рождения</param>
		public Employee(string lastName, string firstName, string middleName, DateTime birthDay) : base(lastName, firstName, middleName, birthDay)
		{
			Experience = 0;
			Position = "";
		}

		/// <summary>
		/// Сотрудник с полями пользователя по-умолчинию
		/// </summary>
		/// <param name="experience">Стаж работы</param>
		/// <param name="position">Должность</param>
		public Employee(int experience, string position)
		{
			Experience = experience;
			Position = position;
		}

		/// <summary>
		/// Сотрудник с заполнеными полями
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="birthDay">День рождения</param>
		/// <param name="experience">Стаж работы</param>
		/// <param name="position">Должность</param>
		public Employee(string lastName, string firstName, string middleName, DateTime birthDay, int experience, string position) : base(lastName, firstName, middleName, birthDay)
		{
			Experience = experience;
			Position = position;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{base.ToString()}\nСтаж работы = {Experience}\nДолжность = {Position}";
		}
	}
}