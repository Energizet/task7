﻿using System;

namespace Norbit.Crm.Krikunov.Task7.Entities
{
	/// <summary>
	/// Пользователь
	/// </summary>
	public class User
	{
		private DateTime _birthDay;

		/// <summary>
		/// Фамилия
		/// </summary>
		public string LastName { get; set; }
		/// <summary>
		/// Имя
		/// </summary>
		public string FirstName { get; set; }
		/// <summary>
		/// Отчество
		/// </summary>
		public string MiddleName { get; set; }

		/// <summary>
		/// День рождения
		/// </summary>
		public DateTime BirthDay
		{
			get => _birthDay;
			set
			{
				if (value > DateTime.Now)
				{
					throw new Exception("День рождения должен быть раньше сегодня");
				}
				_birthDay = value;
			}
		}

		/// <summary>
		/// Возраст
		/// </summary>
		public int Age => (int)(DateTime.Now - BirthDay).TotalDays / 365;

		/// <summary>
		/// Дефолтный пользователь
		/// </summary>
		public User()
		{
			LastName = "";
			FirstName = "";
			MiddleName = "";
			BirthDay = new DateTime(2000, 1, 1);
		}

		/// <summary>
		/// Создание пользователя
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="birthDay">День рождения</param>
		public User(string lastName, string firstName, string middleName, DateTime birthDay)
		{
			LastName = lastName;
			FirstName = firstName;
			MiddleName = middleName;
			BirthDay = birthDay;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"Фамилия = {LastName}\nИмя = {FirstName}\nОтчество = {MiddleName}\nДень рождения = {BirthDay:dd.MM.yyyy}\nВозраст = {Age}";
		}
	}
}