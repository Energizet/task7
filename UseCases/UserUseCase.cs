﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Controllers;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;

namespace Norbit.Crm.Krikunov.Task7.UseCases
{
	/// <summary>
	/// UseCases на пользователя
	/// </summary>
	public class UserUseCase
	{
		/// <summary>
		/// Контроллер (Вывод)
		/// </summary>
		private readonly IUserController _controller;
		/// <summary>
		/// Репозиторий (Ввод)
		/// </summary>
		private readonly IUserRepository _repository;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="controller">Контроллер</param>
		/// <param name="repository">Репозиторий</param>
		public UserUseCase(IUserController controller, IUserRepository repository)
		{
			_controller = controller;
			_repository = repository;
		}

		/// <summary>
		/// Запуск use-case 
		/// </summary>
		public async Task GetAndPrintUser()
		{
			var user = new User
			{
				FirstName = await _repository.GetFirstName(),
				LastName = await _repository.GetLastName(),
				MiddleName = await _repository.GetMiddleName(),
			};
			while (true)
			{
				try
				{
					var birthDay = await _repository.GetBirthDay();
					user.BirthDay = birthDay;
					break;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}

			_controller.PrintUser(user);
		}
	}
}