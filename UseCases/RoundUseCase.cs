﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Controllers;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;

namespace Norbit.Crm.Krikunov.Task7.UseCases
{
	/// <summary>
	/// UseCases на круг
	/// </summary>
	public class RoundUseCase
	{
		/// <summary>
		/// Контроллер (Вывод)
		/// </summary>
		private readonly IRoundController _controller;
		/// <summary>
		/// Репозиторий (Ввод)
		/// </summary>
		private readonly IRoundRepository _repository;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="controller">Контроллер</param>
		/// <param name="repository">Репозиторий</param>
		public RoundUseCase(IRoundController controller, IRoundRepository repository)
		{
			_controller = controller;
			_repository = repository;
		}
		
		/// <summary>
		/// Запуск use-case 
		/// </summary>
		public async Task GetAndPrintRound()
		{
			Round round;
			while (true)
			{
				try
				{
					var radius = await _repository.GetRadius();
					round = new Round(radius);
					break;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
			_controller.PrintRound(round);
		}
	}
}